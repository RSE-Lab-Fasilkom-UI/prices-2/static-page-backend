# Static Page Back End
This tool supports static page management for generated AngularJS
or ReactJS from IFML-UI generator. 


## Environment Preparation
0. Close this repository to your local machine. 
1. Create Virtual Environment:
- Make sure that you have installed PIP (Package Manager for Python)
- Open terminal or command prompt, go to `static-page-backend` directory.
- Install module virtual environment: `pip install virtuaenv`
- Create virtual environment: `virtualenv venv`

2. Activate Virtual Environment
Windows: `venv\Scripts\activate.bat`
Ubuntu or MacOS: `venv\Scripts\activate.bat`
*This step is always required before running the Static Page Backend*

3. Install Python dependencies:
`pip install -r requirements.txt`

## How to run
Make sure that you already run the generated JS application
1. Check the properties.json, change this file if need based on your 
requirements. For example, you could change the product name and selected features.
2. Run `python gev.py`












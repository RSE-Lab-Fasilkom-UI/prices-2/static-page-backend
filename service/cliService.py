import os
import subprocess
from model.Product_Properties import ProductProperties
from settings.constants import PYTHON_EXEC, CLI_DIR, CLI_PATH

def run_refresh_nginx(properties:ProductProperties):
    args = PYTHON_EXEC + [CLI_PATH, '-cfg', os.path.join(CLI_DIR, 'config.ini'), 'refresh_nginx', '%s' % (properties.product_name, )]
    if properties.maintenance_mode:
        args.insert(-1, '-mnt')
    subprocess.call(args)

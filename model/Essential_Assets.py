from interface import implements
from model.interface.serializable import Serializable

class EssentialAssets(implements(Serializable)):

    def __init__(self, logo_url='', banner_url=''):
        self.logo_url = logo_url
        self.banner_url = banner_url

    def serialize(self):
        return {
            'essential_assets' : {
            'logo_url' : self.logo_url,
            'banner_url' : self.banner_url
            }
        }
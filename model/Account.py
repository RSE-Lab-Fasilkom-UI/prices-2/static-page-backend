from interface import implements
from model.interface.serializable import Serializable


class Account(implements(Serializable)):

    def __init__(self, bank_name, account_owner, account_number, **kwargs):
        self.bank_name = bank_name
        self.account_owner = account_owner
        self.account_number = account_number

    def serialize(self):
        return {
            'account': {
                'bank': self.bank_name,
                'owner': self.account_owner,
                'number': self.account_number
            }
        }

from interface import implements
from model.interface.serializable import Serializable

class Contact(implements(Serializable)):

    def __init__(self, address, phone, email, **kwargs):
        self.address = address
        self.phone = phone
        self.email = email
        self.url_dict = kwargs

    def serialize(self):
        return {
            'contact' : {
            'address' : self.address,
            'phone' : self.phone,
            'email' : self.email,
            'url': self.url_dict
            }
        }

